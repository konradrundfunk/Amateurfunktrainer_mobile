# Amateurfunktrainer
## Setting Dart & Flutter up from source 
First make sure you have Flutter installed if you don't want to accept googles tof I recommend using this [guide](https://github.com/flutter/flutter/wiki/Setting-up-the-Engine-development-environment). Make shure you got Flutter added to your system path. 
Secondly you whant to make shure that dart is installed. This [guide](https://github.com/dart-lang/sdk/wiki/Building) helped me a lot also avoiding googles tof. 

## Getting the dependencies 
The dependencies are all sourced from github and a modified repo in order to ditch a bug in flutters dependency management. To get the dependencies run `flutter pub get` in the directory where the project lives in.

## Building for linux target 
Than you can run `flutter doctor` to see what platforms are available. This guide will only demonstrate how to use it on linux with ninja. Please check that flutter doctor outputs a green check mark on the linux toolchain, than run `flutter run` or `flutter run --release`. Select your taget to wich Flutter will compile. 

:sparkles: The App should now start and function as expected.